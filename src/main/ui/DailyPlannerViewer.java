package ui;

import model.Activity;
import model.DailyPlanner;
import model.exceptions.ActivityException;
import model.exceptions.EmptyNameException;
import model.exceptions.InvalidTimeException;
import model.exceptions.NoActivityException;
import persistence.JsonReader;
import persistence.JsonWriter;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.util.*;

// Viewer for the DailyPlanner
public class DailyPlannerViewer {
    // General framework based on the TellerApp implementation:
    // https://github.students.cs.ubc.ca/CPSC210/TellerApp
    private DailyPlanner planner;
    private Scanner input;
    private LocalDate today;

    // EFFECTS: runs the DailyPlannerViewer
    public DailyPlannerViewer() {
        startApp();
    }

    // MODIFIES: this
    // EFFECTS: starts the app, ready to process user input
    public void startApp() {
        boolean toContinue = true;
        String command;

        startPlanner();

        while (toContinue) {
            showOptions();
            command = input.next();
            command = command.toLowerCase();

            if (command.equals("q")) {
                saveDailyPlannerToday();
                toContinue = false;
            } else {
                transferCommand(command);
            }
        }
    }

    // EFFECTS: displays options to choose from
    private void showOptions() {
        System.out.println("\nWelcome to your Daily Planner!");
        System.out.println("Today's date is: " + planner.getDate().toString());
        System.out.println("\nChoose options:");
        insertOptionText("n", "add a new activity");
        insertOptionText("d", "delete an activity");
        insertOptionText("u", "update an activity");
        insertOptionText("v", "view the day");
        insertOptionText("w", "view all activities");
        insertOptionText("l", "load another day from file");
        insertOptionText("s", "save current day to file");
        insertOptionText("q", "quit the app");
    }

    // MODIFIES: this
    // EFFECTS: transfers command to appropriate helper method
    private void transferCommand(String command) {
        Set<String> activityList = new HashSet<>();
        activityList.add("n");
        activityList.add("d");
        activityList.add("u");
        activityList.add("w");

        if (activityList.contains(command)) {
            handleActivityCommands(command);
        } else {
            switch (command) { // basically a BSL cond, implemented as IntelliJ kept warning about it
                case "v":
                    displayDailyPlanner();
                    break;
                case "l":
                    loadOtherDay();
                    break;
                case "s":
                    saveCurrentFile();
                    break;
                default:
                    System.out.println("Command not found - try again");
                    break;
            }
        }
    }

    // MODIFIES: this
    // EFFECTS: acts as a switchboard for choosing activity-related options
    private void handleActivityCommands(String command) {
        switch (command) {
            case "n":
                makeNewActivity();
                break;
            case "d":
                deleteActivity();
                break;
            case "u":
                updateActivity();
                break;
            case "w":
                displayOnlyActivities();
                break;
        }
    }

    // MODIFIES: this
    // EFFECTS: initializes DailyPlanner, tries to load from file for today; if unsuccessful, make new DailyPlanner
    private void startPlanner() {
        today = LocalDate.now();
        // code for checking if file for this date exists based off of:
        // https://stackoverflow.com/a/1816676
        File f = new File("./data/" + today + ".json");
        if (f.exists()) {
            JsonReader r = new JsonReader("./data/" + today.toString() + ".json");
            try {
                planner = r.read();
            } catch (IOException e) {
                System.out.println("Error trying to read DailyPlanner from file! Making new DailyPlanner...");
                planner = new DailyPlanner(today);
            }
        } else {
            planner = new DailyPlanner(today);
        }
        input = new Scanner(System.in);
        input.useDelimiter("\n");
    }

    // MODIFIES: this
    // EFFECTS: prompts user to add a new activity, with name at an appropriate time
    private void makeNewActivity() {
        Activity newActivity;
        String activityName;
        int activityTime = -1;

        System.out.print("Enter name of activity: ");
        activityName = input.next();
        System.out.print("Enter what time to add the activity to: ");

        // check to prevent bad integer inputs based off of:
        // https://stackoverflow.com/a/12832049
        if (input.hasNextInt()) {
            activityTime = input.nextInt();
        }

        try {
            newActivity = new Activity(activityName);
            boolean wasAddSuccessful = planner.addActivity(newActivity, activityTime);
            if (wasAddSuccessful) {
                System.out.println("Activity " + activityName + " has been successfully added at " + activityTime);
            } else {
                System.out.println("Activity add unsuccessful! Activity already exists at " + activityTime);
            }
        } catch (ActivityException e) {
            failedOption("Time or name invalid");
            input.next();
        }
    }

    // MODIFIES: this
    // EFFECTS: checks if an activity at time exists in the DailyPlanner, then delete it if it exists
    private void deleteActivity() {
        int time = -1;

        System.out.print("Time of activity to delete: ");
        if (input.hasNextInt()) {
            time = input.nextInt();
        }

        try {
            planner.removeActivity(time);
            System.out.println("Activity at time " + time + " has been successfully removed!");
        } catch (InvalidTimeException e) {
            failedOption("Invalid time");
        } catch (NoActivityException e) {
            failedOption("There is no activity at " + time);
        }
    }

    // MODIFIES: Activity
    // EFFECTS: check if an activity at time exist, update its name and/or description
    private void updateActivity() {
        int time = -1;

        System.out.println("Please type in the time of the activity to be modified:");
        if (input.hasNextInt()) {
            time = input.nextInt();
        }
        try {
            if (planner.isActivityAtTime(time)) {
                System.out.println("Activity at " + time + " is " + planner.getActivityAtTime(time).getActivityName());
                System.out.println("Press \"n\" to update the name, \"d\" to update the description:");
                String selection = input.next();

                if (selection.equals("n")) {
                    updateName(time);
                } else if (selection.equals("d")) {
                    updateDescription(time);
                } else {
                    failedOption("Invalid option");
                }
            } else {
                failedOption("Nothing is scheduled at that time!");
            }
        } catch (ActivityException e) {
            failedOption("Time invalid!");
        }
    }

    // EFFECTS: displays schedule for the entire day
    private void displayDailyPlanner() {
        // implementation based on HairSalonStarter printBookingsList
        // https://github.com/UBCx-Software-Construction/data-abstraction-lecture-starters
        int startHours = planner.getStartHours();
        int endHours = planner.getEndHours();

        System.out.println("Your daily planner for " + today.toString() + ":");
        for (int i = startHours; i <= endHours; i++) {
            Activity a;
            try {
                a = planner.getActivityAtTime(i);
                System.out.print(i + "hrs: ");
                System.out.println(a.getActivityName());
            } catch (NoActivityException e) {
                System.out.print(i + "hrs: ");
                System.out.println(" No activity! ");
            } catch (InvalidTimeException e) {
                System.out.println("ERROR fetching activity at time " + i);
            }
        }
    }

    // EFFECTS: displays all activities that are scheduled w/ their description
    private void displayOnlyActivities() {
        int startHours = planner.getStartHours();
        int endHours = planner.getEndHours();

        if (planner.getNumActivities() == 0) {
            System.out.println("No activities found!");
        }
        for (int i = startHours; i <= endHours; i++) {
            try {
                if (planner.isActivityAtTime(i)) {
                    System.out.println("Activity at " + i + ": " + planner.getActivityAtTime(i).getActivityName());
                    System.out.println("Description: " + planner.getActivityAtTime(i).getActivityDetails() + "\n");
                }
            } catch (ActivityException e) {
                System.out.println("ERROR fetching activity at " + i);
            }
        }
    }

    // MODIFIES: this
    // EFFECTS: prompts user to type in a file name, loads DailyPlanner from that file if it exists
    private void loadOtherDay() {
        // implementation based off of JsonSerializationDemo
        // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git
        System.out.println("Please type in the FULL file name of another DailyPlanner:");
        String destination = input.next();

        try {
            JsonReader reader = new JsonReader("./data/" + destination);
            planner = reader.read();
            System.out.println("Successfully loaded DailyPlanner for " + planner.getDate().toString());
        } catch (IOException | UncheckedIOException e) {
            failedOption("Could not successfully load from file " + destination);
        }
    }

    // MODIFIES: Activity
    // EFFECTS: prompts user to change the name of the activity
    private void updateName(int time) throws InvalidTimeException, NoActivityException, EmptyNameException {
        String name;

        System.out.println("Current name of activity at " + time + ": "
                + planner.getActivityAtTime(time).getActivityName());
        System.out.print("New name: ");
        name = input.next();

        planner.updateActivityName(time, name);
        System.out.println("Name of activity at " + time + " has been successfully updated to " + name);
    }

    // MODIFIES: Activity
    // EFFECTS: prompts user to change the description of the activity
    private void updateDescription(int time) throws NoActivityException, InvalidTimeException {
        String description;

        System.out.println("Current description of activity at " + time + ": "
                + planner.getActivityAtTime(time).getActivityDetails());
        System.out.print("New description: ");
        description = input.next();

        planner.updateActivityDescription(time, description);
        System.out.println("Description at " + time + " has been successfully updated to " + description);
    }

    // EFFECTS: prints out generic text "description! Press ENTER to continue..." and then prompts for an ENTER input
    private void failedOption(String description) {
        System.out.println(description + "! Press ENTER to continue");
        input.next();
    }

    // REQUIRES: key must be a one-letter lowercase letter
    // EFFECTS: prints out generic text "Press "key" to description"
    private void insertOptionText(String key, String description) {
        System.out.println("\tPress \"" + key + "\" to " + description);
    }

    // EFFECTS: saves the DailyPlanner to file to "./data/$DATE.json"; if unsuccessful, print unsuccessful message
    private void saveDailyPlannerToday() {
        // code based off of JsonSerializationDemo
        // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git
        JsonWriter writer = new JsonWriter("./data/" + planner.getDate().toString() + ".json");
        try {
            writer.open();
            writer.write(planner);
            writer.close();
        } catch (IOException e) {
            System.out.println("Error saving DailyPlanner to file!");
        }
    }

    // EFFECTS: saves the DailyPlanner to file to "./data/$name.json"; if unsuccessful, print error message
    private void saveCurrentFile() {
        // code based off of JsonSerializationDemo
        JsonWriter writer;
        String location;

        System.out.println("Please type in the name you wish to save the DailyPlanner as.");
        location = input.next();

        if (location.equals("")) {
            failedOption("Name can't be \"\"");
        } else {
            try {
                writer = new JsonWriter("./data/" + location + ".json");
                writer.open();
                writer.write(planner);
                writer.close();
                System.out.println("Successfully saved to ./data/" + location + ".json");
            } catch (IOException e) {
                System.out.println("Error saving DailyPlanner to file!");
            }
        }
    }
}
