package ui;

import model.Activity;
import model.DailyPlanner;
import model.EventLog;
import model.exceptions.ActivityException;
import model.exceptions.EmptyNameException;
import model.exceptions.InvalidTimeException;
import model.exceptions.NoActivityException;
import persistence.JsonReader;
import persistence.JsonWriter;
import ui.panels.DailyPlannerPanel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

/**
 * A graphical user interface (GUI) for the DailyPlanner program.
 * @author William X.
 */
public class DailyPlannerUI extends JFrame {
    // fields based off of AlarmControllerUI
    private DailyPlanner dp;
    private DailyPlannerPanel dpPanel;

    private static double INC_AMT = 2.0; // increment font size by amount

    /**
     * Initializes a DailyPlannerUI, sets its layout, menu buttons, and displays
     * the planner.
     */
    public DailyPlannerUI() {
        // implementation based off of AlarmSystem
        // https://github.students.cs.ubc.ca/CPSC210/AlarmSystem.git
        super("Daily Planner");

        setLayout(new GridLayout(2, 1));
        dp = new DailyPlanner(LocalDate.now());
        attemptAutoload();
        dpPanel = new DailyPlannerPanel(dp);
        setupAutosaveBehavior();

        showSplashScreen();

        addMenu();
        addIntroductionText();
        add(dpPanel);
        // pack();
        setSize(new Dimension(350, 600));
        setVisible(true);
    }

    /**
     * Shows the splash screen.
     */
    private void showSplashScreen() {
        // code based off of Java Swing Tutorial SplashDemo
        // https://docs.oracle.com/javase/tutorial/uiswing/examples/misc/SplashDemoProject/src/misc/SplashDemo.java
        try {
            final SplashScreen splashScreen = SplashScreen.getSplashScreen();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // do nothing
            }
            splashScreen.close();

        } catch (NullPointerException e) {
            // do nothing
        }
    }

    /**
     * Sets default close operator, add window listener to try and save before quitting
     */
    private void setupAutosaveBehavior() {
        // code roughly based off of:
        // https://stackoverflow.com/a/45953820
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new AutosaveWindowAdaptor());
    }

    /**
     * Generates the introductory text at the top of window.
     * e.g. "Welcome to the Planner"
     */
    private void addIntroductionText() {
        JPanel textPanel = new JPanel(new GridLayout(2, 1));
        JLabel welcomeLabel = new JLabel("<html><center>Welcome to the Planner!</center><br />"
                + "Today's date is " + dp.getDate().toString() + "</html>", JLabel.CENTER);
        welcomeLabel.setVerticalAlignment(JLabel.TOP);
        welcomeLabel.setFont(new Font("Sans Serif", Font.BOLD, 20));
        textPanel.add(welcomeLabel);
        add(textPanel);
    }

    /**
     * Helper method to generate the menu bar at the top and its elements
     */
    private void addMenu() {
        // implementation based off of AlarmSystem
        // https://github.students.cs.ubc.ca/CPSC210/AlarmSystem.git

        JMenuBar menuBar = new JMenuBar();

        // make a file menu
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic('F');
        addMenuItem(fileMenu, new LoadFileAction(), KeyStroke.getKeyStroke("control O"));
        addMenuItem(fileMenu, new SaveFileAction(), KeyStroke.getKeyStroke("control S"));
        menuBar.add(fileMenu);

        JMenu viewMenu = new JMenu("View");
        viewMenu.setMnemonic('V');
        addMenuItem(viewMenu, new IncreaseFontAction(this), KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, InputEvent.CTRL_DOWN_MASK));
        addMenuItem(viewMenu, new DecreaseFontAction(this), KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_DOWN_MASK));
        menuBar.add(viewMenu);

        // make an activity menu
        JMenu activityMenu = new JMenu("Activity");
        activityMenu.setMnemonic('A');
        addMenuItem(activityMenu, new AddActivityAction(this), KeyStroke.getKeyStroke("control N"));
        addMenuItem(activityMenu, new RemoveActivityAction(this), KeyStroke.getKeyStroke("control X"));
        addMenuItem(activityMenu, new ViewDescriptionAction(this), KeyStroke.getKeyStroke("control D"));
        addMenuItem(activityMenu, new ModifyDescriptionAction(this), KeyStroke.getKeyStroke("control shift D"));
        menuBar.add(activityMenu);

        this.setJMenuBar(menuBar);
    }

    /**
     * Adds a menu item with a handler to menu.
     *
     * @param menu the menu to add the item onto
     * @param action action to be performed if menu is clicked
     * @param accelerator key stroke to be associated as accelerator with
     */
    private void addMenuItem(JMenu menu, AbstractAction action, KeyStroke accelerator) {
        // implementation heavily based off of AlarmControllerUI
        // https://github.students.cs.ubc.ca/CPSC210/AlarmSystem.git

        JMenuItem menuItem = new JMenuItem(action);
        menuItem.setMnemonic(menuItem.getText().charAt(0));
        menuItem.setAccelerator(accelerator);
        menu.add(menuItem);
    }

    /**
     * Attempts to autoload from a file named $DATE.json, changes dp if successful
     */
    private void attemptAutoload() {
        JsonReader reader = new JsonReader("./data/" + dp.getDate().toString() + ".json");
        try {
            dp = reader.read();
            revalidate();
            repaint();
        } catch (IOException e) {
            // do nothing, unsuccessful load
        }
    }

    /**
     * Represents the actions to load a file.
     */
    private class LoadFileAction extends AbstractAction {
        // action class heavily based off of AlarmSystem
        // https://github.students.cs.ubc.ca/CPSC210/AlarmSystem.git

        /**
         * Constructors a load file action with the name "Load File"
         */
        LoadFileAction() {
            super("Load File");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // implementation based off of JFileChooser tutorial
            // https://docs.oracle.com/javase/tutorial/uiswing/components/filechooser.html
            final JFileChooser fc = new JFileChooser("./data");
            fc.setFileFilter(new FileNameExtensionFilter(".json", "json"));
            int returnVal = fc.showOpenDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File toBeLoaded = fc.getSelectedFile();

                JsonReader reader;

                try {
                    reader = new JsonReader(toBeLoaded.getCanonicalPath());
                    dp = reader.read();

                    regenerateDPPanel();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Error loading file!");
                }
            }
        }
    }

    /**
     * Replaces the current dpPanel with a newly generated one from the dp
     */
    private void regenerateDPPanel() {
        DailyPlannerPanel newPanel = new DailyPlannerPanel(dp);
        remove(dpPanel);
        dpPanel = newPanel;
        add(newPanel);
        revalidate();
        repaint();
    }

    /**
     * The actions to be taken for loading a file
     */
    private class SaveFileAction extends AbstractAction {
        // heavily based off of AlarmControllerUI (AlarmSystem)
        // https://github.students.cs.ubc.ca/CPSC210/AlarmSystem.git

        /**
         * Constructs a new save file action named "Save File"
         */
        SaveFileAction() {
            super("Save File");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            final JFileChooser fc = new JFileChooser("./data");
            fc.setSelectedFile(new File(dp.getDate().toString() + ".json"));
            fc.setFileFilter(new FileNameExtensionFilter(".json", "json"));
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File toBeSaved = fc.getSelectedFile();

                JsonWriter writer;
                try {
                    writer = new JsonWriter(toBeSaved.getCanonicalPath());

                    writer.open();
                    writer.write(dp);
                    writer.close();
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(null, "Error saving file!");
                }
            }
        }
    }

    /**
     * A WindowAdaptor that handles autosaving, printing the log, then closing the program.
     */
    private class AutosaveWindowAdaptor extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            // code roughly based off of:
            // https://stackoverflow.com/a/45953820

            JsonWriter writer = new JsonWriter("./data/" + dp.getDate().toString() + ".json");

            try {
                writer.open();
                writer.write(dp);
                writer.close();
            } catch (IOException ioe) {
                // do nothing, unsuccessful autosave
            }

            for (model.Event event : EventLog.getInstance()) {
                System.out.println(event.toString());
            }
            e.getWindow().dispose();
        }
    }

    /**
     * Actions to add a new activity.
     */
    private class AddActivityAction extends AbstractAction {
        DailyPlannerUI parentUI;

        /**
         * Adds a new "Add Activity" action, sets parent UI
         * @param ui the parent UI
         */
        public AddActivityAction(DailyPlannerUI ui) {
            super("Add Activity");
            parentUI = ui;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // based off of DialogDemo.java,
            // https://docs.oracle.com/javase/tutorial/uiswing/examples/zipfiles/components-DialogDemoProject.zip
            String activityName = (String) JOptionPane.showInputDialog(
                    parentUI,
                    "Activity name:", "New Activity",
                    JOptionPane.PLAIN_MESSAGE,
                    null, null, "");

            int activityTime = Integer.parseInt((String) JOptionPane.showInputDialog(
                    parentUI,
                    "Activity time:", "New Activity",
                    JOptionPane.PLAIN_MESSAGE,
                    null, null, ""));

            try {
                Activity newActivity = new Activity(activityName);
                newActivity.setScheduledTime(activityTime);
                dp.addActivity(newActivity, activityTime);
                regenerateDPPanel();
            } catch (EmptyNameException ex) {
                JOptionPane.showMessageDialog(parentUI, "Name cannot be \"\"!");
            } catch (InvalidTimeException ex) {
                JOptionPane.showMessageDialog(parentUI, "Invalid time!");
            }
        }
    }

    /**
     * Actions to remove an activity.
     */
    private class RemoveActivityAction extends AbstractAction {
        DailyPlannerUI parentUI;

        /**
         * Adds menu item named "Remove Activity", sets parent UI
         * @param parentUI the parent ui
         */
        public RemoveActivityAction(DailyPlannerUI parentUI) {
            super("Remove Activity");
            this.parentUI = parentUI;
        }

        /**
         * Prompts user to input time, then removes activity IF FOUND
         * @param e event that occurred
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            // based off of DialogDemo.java
            int activityTime = Integer.parseInt((String) JOptionPane.showInputDialog(
                    parentUI,
                    "Time of activity to be deleted:", "Remove Activity",
                    JOptionPane.PLAIN_MESSAGE,
                    null, null, ""));

            try {
                dp.removeActivity(activityTime);
                regenerateDPPanel();
            } catch (ActivityException ex) {
                JOptionPane.showMessageDialog(parentUI,
                        "Time invalid or activity not found at " + activityTime + "!");
            }
        }
    }

    /**
     * Actions to view a description.
     */
    private class ViewDescriptionAction extends AbstractAction {
        DailyPlannerUI parentUI;

        /**
         * Sets up a new window named "View Description", sets parent UI
         * @param parent the parent ui
         */
        public ViewDescriptionAction(DailyPlannerUI parent) {
            super("View all Activity Descriptions");
            parentUI = parent;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String completeText = "";
            for (int i = DailyPlanner.START_HOURS; i <= DailyPlanner.END_HOURS; i++) {
                try {
                    String name = dp.getActivityAtTime(i).getActivityName();
                    String description = dp.getActivityAtTime(i).getActivityDetails();

                    completeText = completeText + "Activity at " + i + "hrs:\n" + "<html><b>" + name + "</b></html>"
                            + "\n" + "Description: " + description + "\n\n";
                } catch (InvalidTimeException ex) {
                    // should not happen, but if it does popup an error to the screen
                    JOptionPane.showMessageDialog(parentUI, "Error fetching activity! Please report this");
                } catch (NoActivityException ex) {
                    // skip, move on to next activity
                }
            }
            if (completeText.equals("")) {
                JOptionPane.showMessageDialog(parentUI, "No activities in the Daily Planner :(",
                        "Activities", JOptionPane.PLAIN_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(parentUI, completeText, "Activities", JOptionPane.PLAIN_MESSAGE);
            }
        }
    }

    /**
     * Actions to change the description of an activity.
     */
    private class ModifyDescriptionAction extends AbstractAction {
        private DailyPlannerUI parentUI;

        /**
         * Sets up a new window named "Modify Description", sets the parent Ui
         * @param parent the parent ui
         */
        public ModifyDescriptionAction(DailyPlannerUI parent) {
            super("Modify Description");
            parentUI = parent;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int activityTime = Integer.parseInt((String) JOptionPane.showInputDialog(
                    parentUI, "Time of activity:", "Change Description",
                    JOptionPane.PLAIN_MESSAGE, null, null, ""));

            try {
                String newDescription = (String) JOptionPane.showInputDialog(
                        parentUI, "New description:", "Change Description",
                        JOptionPane.PLAIN_MESSAGE, null, null,
                        dp.getActivityAtTime(activityTime).getActivityDetails());

                dp.updateActivityDescription(activityTime, newDescription);
            } catch (ActivityException ae) {
                JOptionPane.showMessageDialog(parentUI, "Error! No activity at time or time invalid!");
            }
        }
    }
    
    /**
     * Actions to increase the font size of the entire DailyPlanner,
     * by adding a constant to the font size of every component.
     */
    private class IncreaseFontAction extends AbstractAction {
        private DailyPlannerUI parentUI;

        public IncreaseFontAction(DailyPlannerUI dpui) {
            super("Increase Font Size");
            parentUI = dpui;
        }

        /**
         * Helper methods to recursively change the font size of each panel.
         */
        private void increaseFontSizes(Component[] components) {
            for (Component c : components) {
                c.setFont(c.getFont().deriveFont((float) (c.getFont().getSize() + INC_AMT)));
                if (c instanceof Container) {
                    increaseFontSizes(((Container) c).getComponents());
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            increaseFontSizes(parentUI.getRootPane().getComponents());
        }
    }

    /**
     * Action to decrease the font size of the entire DailyPlanner,
     * by subtracting a constant to the font size of every component.
     */
    private class DecreaseFontAction extends AbstractAction {
        private DailyPlannerUI parentUI;

        public DecreaseFontAction(DailyPlannerUI dpui) {
            super("Decrease Font Size");
            parentUI = dpui;
        }

        private void decreaseFontSizes(Component[] components) {
            for (Component c : components) {
                c.setFont(c.getFont().deriveFont((float) (c.getFont().getSize() - INC_AMT)));
                if (c instanceof Container) {
                    decreaseFontSizes(((Container) c).getComponents());
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            decreaseFontSizes(parentUI.getRootPane().getComponents());
        }
    }
}
