package ui.panels;

import model.DailyPlanner;
import model.exceptions.ActivityException;

import javax.swing.*;
import java.awt.*;

// A panel for just the DailyPlanner's information.
public class DailyPlannerPanel extends JPanel {
    private final DailyPlanner dp;

    // EFFECTS: creates a new GridLayout and empty DailyPlannerPanel
    public DailyPlannerPanel(DailyPlanner dp) {
        super(new GridLayout(DailyPlanner.END_HOURS - DailyPlanner.START_HOURS + 2, 1));
        this.dp = dp;

        addHeaders();
        initializeActivities();
    }

    // MODIFIES: this
    // EFFECTS: adds headers to the DailyPlannerPanel with name and time
    private void addHeaders() {
        JLabel timeLabel = new JLabel("<html><u>Time</u></html>");
        JLabel nameLabel = new JLabel("<html><u>Activity Name</u></html>");
        JPanel headerPanel = new JPanel(new GridLayout(1,2));

        timeLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        headerPanel.add(timeLabel);
        headerPanel.add(nameLabel);
        add(headerPanel);
    }

    // MODIFIES: this
    // EFFECTS: adds all the times and activities to the DailyPlannerPanel
    public void initializeActivities() {
        for (int i = DailyPlanner.START_HOURS; i <= DailyPlanner.END_HOURS; i++) {
            ActivityPanel ap;
            try {
                ap = new ActivityPanel(i, dp.getActivityAtTime(i), dp);
                ap.setAlignmentX(JPanel.CENTER_ALIGNMENT);
                add(ap);
            } catch (ActivityException e) {
                ap = new ActivityPanel(i, dp);
                ap.setAlignmentX(JPanel.CENTER_ALIGNMENT);
                add(ap);
            }
        }
    }
}
