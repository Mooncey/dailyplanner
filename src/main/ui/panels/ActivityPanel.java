package ui.panels;

import model.Activity;
import model.DailyPlanner;
import model.exceptions.ActivityException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// A panel for one activity's name and its time
public class ActivityPanel extends JPanel implements ActionListener {
    private Activity activity;
    private JTextField activityName;
    private final JLabel activityTime;
    private final DailyPlanner dp;
    private final int time;

    // EFFECTS: starts a new layout, creates time label, generates JTextField
    public ActivityPanel(int time, Activity a, DailyPlanner dp) {
        super(new GridLayout(1, 2));
        activityTime = new JLabel(time + " hrs:");
        activityName = generateTextField(a);
        this.activity = a;
        this.time = time;
        this.dp = dp;

        activityTime.setHorizontalAlignment(JLabel.CENTER);
        activityName.setHorizontalAlignment(JLabel.CENTER);
        activityName.addActionListener(this);

        add(activityTime);
        add(activityName);
    }

    // EFFECTS: starts a new layout, with empty JTextField for no activity
    public ActivityPanel(int time, DailyPlanner dp) {
        super(new GridLayout(1, 2));
        this.time = time;
        this.dp = dp;
        activityTime = new JLabel(time + " hrs:");
        activityName = new JTextField();
        activityTime.setHorizontalAlignment(JLabel.CENTER);
        activityName.setHorizontalAlignment(JLabel.CENTER);
        activityName.addActionListener(this);
        add(activityTime);
        add(activityName);
    }

    // MODIFIES: this
    // EFFECTS: changes the Activity associated with this panel, generates a new text field w/ updated information
    public void changeActivity(Activity a) {
        activity = a;
        activityName = generateTextField(a);
        revalidate();
        repaint();
    }

    // EFFECTS: creates a text field with the name of the activity
    public JTextField generateTextField(Activity a) {
        return new JTextField(a.getActivityName());
    }

    // MODIFIES: dp
    // EFFECTS: if activity doesn't already exist at time, make one; update name of activity; remove activity if
    //          "" is inputted
    @Override
    public void actionPerformed(ActionEvent evt) {
        String textContent = activityName.getText();

        if (activity == null) {
            try {
                activity = new Activity(textContent);
                dp.addActivity(activity, time);
            } catch (ActivityException e) {
                // unsuccessful activity add, don't do anything
            }
        }
        if (textContent.equals("")) {
            try {
                dp.removeActivity(time);
            } catch (ActivityException e) {
                // unsuccessful activity remove, don't do anything
            }
        }
        activity.setActivityName(textContent);
    }
}
