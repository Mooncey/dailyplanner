package model;

import model.exceptions.EmptyNameException;
import model.exceptions.InvalidTimeException;
import model.exceptions.NoActivityException;
import org.json.JSONArray;
import org.json.JSONObject;
import persistence.Writable;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * A daily planner with its day, scheduled activities, and timeslots for those activities
 */
public class DailyPlanner implements Writable {
    private Map<Integer, Activity> activityMap;
    public static final int START_HOURS = 8;
    public static final int END_HOURS = 23;
    private LocalDate todaysDate;

    /**
     * Initializes the activity tracker, activities from START_HOURS to END_HOURS are
     * all set to null by default, sets the date
     * @param date the date to be set to
     */
    public DailyPlanner(LocalDate date) {
        todaysDate = date;
        activityMap = new LinkedHashMap<>(); // to preserve order

        // Idea for this implementation came from HairSalonStarter,
        // https://github.com/UBCx-Software-Construction/data-abstraction-lecture-starters
        for (int i = START_HOURS; i <= END_HOURS; i++) {
            activityMap.put(i, null);
        }
    }

    /**
     * Adds activity at specified time
     * @param a activity to be added
     * @param time time to schedule this activity
     * @return true if successful, false otherwise
     * @throws InvalidTimeException if time specified for the activity is not within [START_HOURS, END_HOURS]
     */
    public boolean addActivity(Activity a, int time) throws InvalidTimeException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }
        if (activityMap.get(time) == null) {
            activityMap.put(time, a);
            a.setScheduledTime(time);
            EventLog.getInstance().logEvent(new Event("Added activity " + a.getActivityName() + " at "
                    + time));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes activity at specified time.
     * @param time time to remove activity from
     * @throws InvalidTimeException time outside [START_HOURS, END_HOURS]
     * @throws NoActivityException non activity found at time
     */
    public void removeActivity(int time) throws InvalidTimeException, NoActivityException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }
        if (activityMap.get(time) == null) {
            throw new NoActivityException();
        }
        activityMap.get(time).setScheduledTime(-1);
        activityMap.remove(time);
        activityMap.put(time, null);
        EventLog.getInstance().logEvent(new Event("Removed activity at: " + time));
    }

    /**
     * Updates an activity at a time to a new name
     * @param time time to activity to modify
     * @param name name of activity to be changed to
     * @throws InvalidTimeException time is outside [START_HOURS, END_HOURS]
     * @throws NoActivityException no activity found at time
     * @throws EmptyNameException new name is ""
     */
    public void updateActivityName(int time, String name)
            throws InvalidTimeException, NoActivityException, EmptyNameException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }
        if (activityMap.get(time) == null) {
            throw new NoActivityException();
        }
        if (name.isEmpty()) {
            throw new EmptyNameException();
        }

        Activity toBeModified = activityMap.get(time);
        toBeModified.setActivityName(name);
        EventLog.getInstance().logEvent(new Event("Changed activity name at: " + time + " to: " + name));
    }

    /**
     * Updates an activity's description at the specified time
     * @param time time of activity
     * @param description new description for activity
     * @throws InvalidTimeException time is outside [START_HOURS, END_HOURS]
     * @throws NoActivityException there is no activity at time
     */
    public void updateActivityDescription(int time, String description)
            throws InvalidTimeException, NoActivityException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }
        if (activityMap.get(time) == null) {
            throw new NoActivityException();
        }

        Activity toBeModified = activityMap.get(time);
        toBeModified.setActivityDetails(description);
        EventLog.getInstance().logEvent(new Event("Changed details of activity at " + time + " to " + description));
    }

    public LocalDate getDate() {
        return todaysDate;
    }

    /**
     *
     * @return number of activities in the DailyPlanner
     */
    public int getNumActivities() {
        int count = 0;
        Set<Map.Entry<Integer, Activity>> activities = activityMap.entrySet();
        for (Map.Entry<Integer, Activity> e : activities) {
            if (e.getValue() != null) {
                count++;
            }
        }
        return count;
    }

    /**
     *
     * @return total hours of all activities in this
     */
    public int getTotalHours() {
        // currently, same implementation as count, but will change in the future! (when hrs can change)
        int count = 0;
        Set<Map.Entry<Integer, Activity>> activities = activityMap.entrySet();

        for (Map.Entry<Integer, Activity> e : activities) {
            if (e.getValue() != null) {
                count++;
            }
        }
        return count;
    }

    public int getStartHours() {
        return START_HOURS;
    }

    public int getEndHours() {
        return END_HOURS;
    }

    /**
     * Gets activity at a specified time, assuming it exists
     * @param time time of activity
     * @return activity at time
     * @throws InvalidTimeException time is outside [START_HOURS, END_HOURS]
     * @throws NoActivityException activity not found at time
     */
    public Activity getActivityAtTime(int time) throws InvalidTimeException, NoActivityException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }
        if (activityMap.get(time) == null) {
            throw new NoActivityException();
        }

        return activityMap.get(time);
    }

    /**
     * Searches for an activity with name.
     * @param name name to search for
     * @return true if activity with name was found, false otherwise
     */
    public boolean isActivityInPlanner(String name) {
        Set<Map.Entry<Integer, Activity>> activities = activityMap.entrySet();
        for (Map.Entry<Integer, Activity> e : activities) {
            Activity a = e.getValue();
            if (a != null) {
                if (a.getActivityName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Searches for an activity at time.
     * @param time time to search at for an activity
     * @return true if activity at time was found, false otherwise
     * @throws InvalidTimeException time is outside [START_HOURS, END_HOURS]
     */
    public boolean isActivityAtTime(int time) throws InvalidTimeException {
        if (!(START_HOURS <= time && time <= END_HOURS)) {
            throw new InvalidTimeException();
        }

        return activityMap.get(time) != null;
    }

    @Override
    public JSONObject toJson() {
        // implementation based off of JsonSerializationDemo
        // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git
        JSONObject json = new JSONObject();
        json.put("date", todaysDate.toString());
        json.put("activities", activitiesToJson());
        return json;
    }

    /**
     * Converts activities in the DailyPlanner
     * @return activities in DailyPlanner as a JSON array
     */
    private JSONArray activitiesToJson() {
        // implementation based off of JsonSerializationDemo
        JSONArray jsonArray = new JSONArray();

        Set<Map.Entry<Integer, Activity>> activities = activityMap.entrySet();

        for (Map.Entry<Integer, Activity> e : activities) {
            Activity a = e.getValue();
            if (a != null) {
                jsonArray.put(a.toJson());
            }
        }
        return jsonArray;
    }
}
