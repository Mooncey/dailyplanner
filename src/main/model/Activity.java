package model;

import model.exceptions.EmptyNameException;
import org.json.JSONObject;
import persistence.Writable;

/**
 * An activity with a name, optional details
 */
public class Activity implements Writable {
    private String activityName;
    private String activityDetails;
    private int scheduledTime;

    /**
     * Sets name of activity, sets blank ActivityDetails
     * @param name name of the activity
     * @throws EmptyNameException if "" is passed as name
     */
    public Activity(String name) throws EmptyNameException {
        if (name.isEmpty()) {
            throw new EmptyNameException();
        }
        activityName = name;
        activityDetails = "";
        scheduledTime = -1;
    }

    public void setActivityDetails(String details) {
        activityDetails = details;
    }

    public void setActivityName(String name) {
        activityName = name;
    }

    public void setScheduledTime(int time) {
        scheduledTime = time; // note: -1 means activity has not been scheduled
    }

    public String getActivityName() {
        return activityName;
    }

    public String getActivityDetails() {
        return activityDetails;
    }

    @Override
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("name", activityName);
        json.put("time", scheduledTime);
        json.put("desc", activityDetails);
        return json;
    }
}
