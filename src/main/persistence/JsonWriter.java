package persistence;

import model.DailyPlanner;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * A writer that writes a DailyPlanenr + its activities onto JSON to file
 *
 * General structure based off of JsonSerializationDemo (link only works from UBC):
 * https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git
 */
public class JsonWriter {
    private static final int TAB = 4;
    private PrintWriter writer;
    private String dest;

    /**
     * Constructs writer, sets destination
     * @param destination destination of the writer
     */
    public JsonWriter(String destination) {
        dest = destination;
    }

    /**
     * Opens writer to be ready for writing
     * @throws FileNotFoundException if there was a problem opening destination file
     */
    public void open() throws FileNotFoundException {
        // implementation from JsonSerializationDemo
        writer = new PrintWriter(new File(dest));
    }

    /**
     * Writes JSON representation of DailyPlanner to file
     * @param dp DailyPlanner to be written to the file
     */
    public void write(DailyPlanner dp) {
        // implementation from JsonSerializationDemo
        JSONObject json = dp.toJson();
        saveToFile(json.toString(TAB));
    }

    /**
     * Closes the writer
     */
    public void close() {
        // from JsonSerializationDemo
        writer.close();
    }

    /**
     * Writes json in string form to file
     * @param json the text to be printed to the writer for the file saving
     */
    // MODIFIES: this
    // EFFECTS: writes json as a string to file
    private void saveToFile(String json) {
        writer.print(json);
    }
}
