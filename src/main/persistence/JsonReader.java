package persistence;

import model.Activity;
import model.DailyPlanner;
import model.exceptions.ActivityException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * A reader that fetches a DailyPlanner from a file containing JSON data
 *
 * General structure based off of JsonSerializationDemo:
 * https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git (link only works from UBC)
 */
public class JsonReader {
    private String source;

    /**
     * Constructs a new reader, sets the source destination.
     * @param destination the source destination
     */
    public JsonReader(String destination) {
        this.source = destination;
    }

    /**
     * Reads the DailyPlanner from the source file
     * @return DailyPlanner reconstructed from file specified by the source
     * @throws IOException if there was a file-read error
     */
    public DailyPlanner read() throws IOException {
        // Implementation from JsonSerializationDemo
        String data = readFile(source);
        JSONObject json = new JSONObject(data);
        return parseDailyPlanner(json);
    }

    /**
     * Fetches the file from source.
     * @param source the source of the file
     * @return a string containing the contents of the source file
     * @throws IOException if there was a file-read error
     */
    private String readFile(String source) throws IOException {
        // Implementation from JsonSerializationDemo
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<java.lang.String> stream = Files.lines(Paths.get(source), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s));
        }

        return contentBuilder.toString();
    }

    /**
     * Parses DailyPlanner from JSON object, returns corresponding DailyPlanner
     * @param json the JSONObject to extract data from
     * @return DailyPlanner recreated from JSONObject information
     */
    // EFFECTS: parses DailyPlanner from JSON object, returns DailyPlanner
    private DailyPlanner parseDailyPlanner(JSONObject json) {
        // Implementation based off of JsonSerializationDemo
        LocalDate date = LocalDate.parse(json.getString("date"));
        DailyPlanner dp = new DailyPlanner(date);
        addActivities(dp, json);
        return dp;
    }

    /**
     * Parces activities from JSON object, adds activities to the Daily Planner
     * @param dp Daily Planner to add activities to
     * @param json JSONObject containing the activities
     */
    private void addActivities(DailyPlanner dp, JSONObject json) {
        // Implementation from JsonSerializationDemo
        JSONArray jsonArray = json.getJSONArray("activities");
        for (Object js : jsonArray) {
            try {
                JSONObject nextActivity = (JSONObject) js;
                addActivity(dp, nextActivity);
            } catch (ActivityException ignored) {
                // skip this activity
            }
        }
    }

    /**
     * Parses one activity from JSON object, adds to Daily Planner
     * @param dp Daily Planner to add activity to
     * @param json JSONObject that contains the ACTIVITY
     * @throws ActivityException any Activity invariants are invalid (see {@link model.Activity})
     */
    private void addActivity(DailyPlanner dp, JSONObject json) throws ActivityException {
        // Implementation based off of JsonSerializationDemo
        String name = json.getString("name");
        int time = json.getInt("time");
        String desc = json.getString("desc");

        Activity newActivity = new Activity(name);
        newActivity.setActivityDetails(desc);
        dp.addActivity(newActivity, time);
    }
}
