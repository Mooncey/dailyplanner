package persistence;

import org.json.JSONObject;

public interface Writable {
    // idea for Writable from JsonSerializationDemo
    // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git

    /**
     * @return the JSON object for this Writable
     */
    JSONObject toJson();
}
