package persistence;

import model.Activity;

import static org.junit.jupiter.api.Assertions.assertEquals;

// Tests for the Activities in a json
public class JsonTest {
    protected void checkActivity(String name, String desc, Activity a) {
        assertEquals(name, a.getActivityName());
        assertEquals(desc, a.getActivityDetails());
    }
}
