package persistence;

import model.Activity;
import model.DailyPlanner;
import model.exceptions.ActivityException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

// Tests for the JsonWriter
public class JsonWriterTest extends JsonTest {
    // Tests based off of idea from JsonSerializationDemo
    // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git

    @Test
    public void testWriterInvalidFile() {
        try {
            DailyPlanner dp = new DailyPlanner(LocalDate.now());
            JsonWriter writer = new JsonWriter("./data/a\0:badfileName.json");
            writer.open();
            fail("IOException not thrown");
        } catch (IOException e) {
            // pass
        }
    }

    @Test
    public void testWriterDate20210630() {
        try {
            DailyPlanner dp = new DailyPlanner(LocalDate.parse("2021-06-30"));
            JsonWriter writer = new JsonWriter("./data/testWriterDate20210631.json");
            writer.open();
            writer.write(dp);
            writer.close();

            JsonReader r = new JsonReader("./data/testWriterDate20210631.json");
            dp = r.read();
            assertEquals("2021-06-30", dp.getDate().toString());
            assertEquals(0, dp.getNumActivities());
        } catch (IOException e) {
            fail("Unexpected IOException");
        }
    }

    @Test
    public void testWriterThreeActivities() {
        try {
            Activity a1 = new Activity("trip");
            Activity a2 = new Activity("school");
            Activity a3 = new Activity("home");
            a1.setActivityDetails("going somewhere");
            DailyPlanner dp = new DailyPlanner(LocalDate.parse("2020-10-31"));
            dp.addActivity(a1, 10);
            dp.addActivity(a2, 18);
            dp.addActivity(a3, 21);

            JsonWriter writer = new JsonWriter("./data/testWriterThreeActivities.json");
            writer.open();
            writer.write(dp);
            writer.close();

            JsonReader reader = new JsonReader("./data/testWriterThreeActivities.json");
            dp = reader.read();
            assertEquals("2020-10-31", dp.getDate().toString());
            assertEquals(3, dp.getNumActivities());
            checkActivity("trip", "going somewhere", dp.getActivityAtTime(10));
            checkActivity("school", "", dp.getActivityAtTime(18));
            checkActivity("home", "", dp.getActivityAtTime(21));
        } catch (IOException e) {
            fail("Unexpected IOException");
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }
}
