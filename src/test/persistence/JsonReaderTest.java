package persistence;

import model.Activity;
import model.DailyPlanner;
import model.exceptions.ActivityException;
import model.exceptions.InvalidTimeException;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

// Tests for the JsonReader
public class JsonReaderTest extends JsonTest {
    // Tests based off of JsonSerializationDemo,
    // https://github.students.cs.ubc.ca/CPSC210/JsonSerializationDemo.git

    @Test
    public void testReadingNoFile() {
        JsonReader r = new JsonReader("./data/null.json");
        try {
            DailyPlanner dp = r.read();
            fail("No IOException was thrown");
        } catch (IOException ignored) {
            // pass
        }
    }

    @Test
    public void testReadingDPDate20200301() {
        JsonReader r = new JsonReader("./data/testReadingFileNameOnly.json");
        try {
            DailyPlanner dp = r.read();
            assertEquals("2020-03-01", dp.getDate().toString());
            assertEquals(0, dp.getNumActivities());
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
        }
    }

    @Test
    public void testReadingDPWithActivities() {
        JsonReader r = new JsonReader("./data/testReadingDPWithActivities.json");
        try {
            DailyPlanner dp = r.read();
            assertEquals("2020-03-01", dp.getDate().toString());
            assertEquals(3, dp.getNumActivities());
            assertTrue(dp.isActivityAtTime(10));
            assertTrue(dp.isActivityAtTime(15));
            assertTrue(dp.isActivityAtTime(18));

            checkActivity("trip", "going to North Vancouver", dp.getActivityAtTime(10));
            checkActivity("homework", "", dp.getActivityAtTime(15));
            checkActivity("dinner", "yummy spaghetti", dp.getActivityAtTime(18));
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
        } catch (ActivityException e) {
            fail("Unexpected ActivityException thrown");
        }
    }

    @Test
    public void testReadDPSkipInvalidActivities() {
        JsonReader r = new JsonReader("./data/testReadDPSkipInvalidActivities.json");
        try {
            DailyPlanner dp = r.read();
            assertEquals("2020-03-15", dp.getDate().toString());
            assertEquals(2, dp.getNumActivities());
            assertTrue(dp.isActivityAtTime(10));
            assertFalse(dp.isActivityAtTime(15));
            assertTrue(dp.isActivityAtTime(18));

            checkActivity("trip", "going to North Vancouver", dp.getActivityAtTime(10));
            checkActivity("dinner", "yummy spaghetti", dp.getActivityAtTime(18));
        } catch (IOException e) {
            fail("Unexpected IOException");
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }
}
