package model;

import model.exceptions.EmptyNameException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

// Tests for the Activity class
public class ActivityTest {
    Activity testActivity;

    @BeforeEach
    public void setUp() {
        try {
            testActivity =  new Activity("test");
        } catch (EmptyNameException e) {
            fail("Unexpected EmptyNameException");
        }
    }

    @Test
    public void testSettersAndGetters() {
        testActivity.setActivityDetails("some details");
        assertEquals("test", testActivity.getActivityName());
        assertEquals("some details", testActivity.getActivityDetails());

        testActivity.setActivityName("no longer test");
        testActivity.setActivityDetails("updated details!");
        assertEquals("no longer test", testActivity.getActivityName());
        assertEquals("updated details!", testActivity.getActivityDetails());
    }

    @Test
    public void testConstructorException() {
        Activity badActivity;
        try {
            badActivity = new Activity("");
            fail("Constructor did not throw EmptyNameException");
        } catch (EmptyNameException ignored) {
        }
    }
}
