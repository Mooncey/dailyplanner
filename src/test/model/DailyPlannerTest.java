package model;

import model.exceptions.ActivityException;
import model.exceptions.EmptyNameException;
import model.exceptions.InvalidTimeException;
import model.exceptions.NoActivityException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

// Tests for the DailyPlanner
class DailyPlannerTest {
    DailyPlanner myPlanner;
    LocalDate aDate;

    @BeforeEach
    public void setUp() {
        aDate = LocalDate.now();
        myPlanner = new DailyPlanner(aDate);
    }

    @Test
    public void testConstructor() {
        assertEquals(0, myPlanner.getTotalHours());
        assertEquals(aDate, myPlanner.getDate());
        assertEquals(0, myPlanner.getNumActivities());
    }

    @Test
    public void testAddActivity() {
        Activity newActivity;

        try {
            newActivity = new Activity("test");
            assertTrue(myPlanner.addActivity(newActivity, 10));
            assertEquals(1, myPlanner.getNumActivities());
            assertEquals(newActivity, myPlanner.getActivityAtTime(10));
            assertEquals(1, myPlanner.getTotalHours());

            myPlanner.addActivity(newActivity, myPlanner.getStartHours());
            assertEquals(2, myPlanner.getNumActivities());
            assertEquals(newActivity, myPlanner.getActivityAtTime(myPlanner.getStartHours()));
            assertEquals(2, myPlanner.getTotalHours());

            myPlanner.addActivity(newActivity, myPlanner.getEndHours());
            assertEquals(3, myPlanner.getNumActivities());
            assertEquals(3, myPlanner.getTotalHours());

            try {
                myPlanner.addActivity(newActivity, -10);
                fail("addActivity did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass time too small
            }
            try {
                myPlanner.addActivity(newActivity, 1000);
                fail("addActivity did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass time too large
            }
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testOverrideActivity() {
        try {
            Activity newActivity = new Activity("test");
            Activity otherActivity = new Activity("2");

            assertTrue(myPlanner.addActivity(newActivity, 10));
            assertFalse(myPlanner.addActivity(otherActivity, 10));
            assertEquals(1, myPlanner.getNumActivities());
            assertEquals(newActivity, myPlanner.getActivityAtTime(10));

            myPlanner.removeActivity(10);
            assertTrue(myPlanner.addActivity(otherActivity, 10));
            assertEquals(1, myPlanner.getNumActivities());
            assertEquals(otherActivity, myPlanner.getActivityAtTime(10));
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testRemoveActivityByHour() {
        Activity newActivity;
        try {
            newActivity = new Activity("test");
            myPlanner.addActivity(newActivity, 10);
            myPlanner.addActivity(newActivity, 14);

            myPlanner.removeActivity(14);
            assertEquals(1, myPlanner.getNumActivities());
            assertEquals(1, myPlanner.getTotalHours());

            myPlanner.removeActivity(10);
            assertEquals(0, myPlanner.getNumActivities());
            assertEquals(0, myPlanner.getTotalHours());
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }

        try {
            myPlanner.removeActivity(10);
            fail("removeActivity did not throw NoActivityException");
        } catch (NoActivityException ignored) {
            // pass
        } catch (InvalidTimeException e) {
            fail("Unexpected InvalidTimeException");
        }
        try {
            myPlanner.removeActivity(-10);
            fail("removeActivity did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass
        } catch (NoActivityException e) {
            fail("Unexpected NoActivityException");
        }
        try {
            myPlanner.removeActivity(1000);
            fail("removeActivity did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass
        } catch (NoActivityException e) {
            fail("Unexpected NoActivityException");
        }
    }

    @Test
    public void testRemoveActivitiesAtAndCloseToEdge() {
        try {
            Activity newActivity = new Activity("test");

            myPlanner.addActivity(newActivity, myPlanner.getStartHours());
            myPlanner.addActivity(newActivity, myPlanner.getStartHours() + 1);
            myPlanner.addActivity(newActivity, myPlanner.getEndHours());
            myPlanner.addActivity(newActivity, myPlanner.getEndHours() - 1);

            myPlanner.removeActivity(myPlanner.getStartHours());
            assertEquals(myPlanner.getNumActivities(), 3);
            myPlanner.removeActivity(myPlanner.getEndHours());
            assertEquals(myPlanner.getNumActivities(), 2);
            myPlanner.removeActivity(myPlanner.getStartHours() + 1);
            assertEquals(myPlanner.getNumActivities(), 1);
            myPlanner.removeActivity(myPlanner.getEndHours() - 1);
            assertEquals(myPlanner.getNumActivities(), 0);
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testIsActivityInPlanner() {
        try {
            Activity a1 = new Activity("hi");
            Activity a2 = new Activity("hey");

            myPlanner.addActivity(a1, 18);
            myPlanner.addActivity(a2, 18);
            assertTrue(myPlanner.isActivityInPlanner("hi"));
            assertFalse(myPlanner.isActivityInPlanner("hey"));

            myPlanner.addActivity(a2, 10);
            assertTrue(myPlanner.isActivityInPlanner("hi"));
            assertTrue(myPlanner.isActivityInPlanner("hey"));

            myPlanner.removeActivity(18);
            assertFalse(myPlanner.isActivityInPlanner("hi"));
            assertTrue(myPlanner.isActivityInPlanner("hey"));

            myPlanner.removeActivity(10);
            assertFalse(myPlanner.isActivityInPlanner("hey"));
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testIsActivityAtTime() {
        try {
            Activity a1 = new Activity("test");

            myPlanner.addActivity(a1, 10);
            assertTrue(myPlanner.isActivityAtTime(10));

            myPlanner.removeActivity(10);
            assertFalse(myPlanner.isActivityAtTime(10));
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testHourGetters() {
        assertEquals(myPlanner.START_HOURS, myPlanner.getStartHours());
        assertEquals(myPlanner.END_HOURS, myPlanner.getEndHours());
    }

    @Test
    public void testUpdateActivityNameAndDescription() {
        try {
            Activity a1 = new Activity("test");
            myPlanner.addActivity(a1, 10);

            try {
                myPlanner.updateActivityName(-10, "invalid time");
                fail("updateActivityName did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass
            }
            try {
                myPlanner.updateActivityName(1000, "invalid time");
                fail("updateActivityName did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass
            }
            try {
                myPlanner.updateActivityName(9, "no activity");
                fail("updateActivityName did not throw NoActivityException");
            } catch (NoActivityException ignored) {
                // pass
            }
            try {
                myPlanner.updateActivityName(10, "");
                fail("updateActivityName did not throw EmptyNameException");
            } catch (EmptyNameException ignored) {
                // pass
            }

            myPlanner.updateActivityName(10, "new name");
            String name = myPlanner.getActivityAtTime(10).getActivityName();
            assertEquals("new name", name);
            assertNotEquals("test", name);

            try {
                myPlanner.updateActivityDescription(-10, "invalid time");
                fail("updateActivityDescription did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass
            }
            try {
                myPlanner.updateActivityDescription(1000, "invalid time");
                fail("updateActivityDescription did not throw InvalidTimeException");
            } catch (InvalidTimeException ignored) {
                // pass
            }

            try {
                myPlanner.updateActivityDescription(DailyPlanner.END_HOURS, "no activity");
                fail("updateActivityDescription did not throw NoActivityException");
            } catch (NoActivityException ignored) {
                // pass
            }

            myPlanner.updateActivityDescription(10, "yay for some details");
            String details = myPlanner.getActivityAtTime(10).getActivityDetails();
            assertEquals("yay for some details", details);
        } catch (ActivityException e) {
            fail("Unexpected ActivityException");
        }
    }

    @Test
    public void testGetActivityAtTimeExceptions() {
        try {
            myPlanner.getActivityAtTime(10);
            fail("getActivityAtTime did not throw NoActivityException");
        } catch (NoActivityException ignored) {
            // pass
        } catch (Exception other) {
            fail("getActivityAtTime did not throw NoActivityException");
        }
        try {
            myPlanner.getActivityAtTime(-1);
            fail("getActivityAtTime did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass
        } catch (Exception other) {
            fail("getActivityAtTime did not throw InvalidTimeException");
        }
        try {
            myPlanner.getActivityAtTime(5000);
            fail("getActivityAtTime did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass
        } catch (Exception other) {
            fail("getActivityAtTime did not throw InvalidTimeException");
        }
    }

    @Test
    public void testIsActivityAtTimeExceptions() {
        try {
            myPlanner.isActivityAtTime(-1);
            fail("isActivityAtTime did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass too small time
        }
        try {
            myPlanner.isActivityAtTime(1000);
            fail("isActivityAtTime did not throw InvalidTimeException");
        } catch (InvalidTimeException ignored) {
            // pass too large time
        }
    }
}