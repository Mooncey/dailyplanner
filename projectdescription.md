# Personal Project: Daily Planner
## Motivation
Whenever I have many things to do (especially with many
courses!), I always find it helpful to spontaneously plan out my days.
Whenever I write down something in my ToDo list, I always write down
a rough time interval. Then, every morning I like to use a whiteboard
to plan out my day by looking at the ToDo list and scheduling 
activities accordingly.

While that strategy worked well for a COVID-19 online school 
environment where I was ***always*** home, unfortunately in-person
classes introduce a few problems with this workflow:
- I have no access to the whiteboard when I'm on campus
- Even if I did have access through a picture, I wouldn't be able
  to edit it throughout the day

Over the past few months, I've tried searching for a virtual solution
but none seem to exist that does what I want!

## Project Description
For my personal project, I'd like to build a daily planner that 
allows someone to input their activities and place them onto a 
virtual schedule that can be easily edited throughout the day.
I also want this application to be able to carry over activities
from the previous day that I could mark as "incomplete" or
"priority", making it easy to resume where I previously left off.
Finally, this application should allow for recurring activities
that are permanently part of the schedule for any given weekday, to
be updated automatically.
I would like this application to be useful to anyone who enjoys
planning out their day in a detailed manner like I do. Even if
someone doesn't like planning out their day in detail, having an
easily-updatable schedule sounds like it would probably be a useful
feature for many people.


## User Stories
- As a user, I want to be able to add activities at specific times
  to my daily planner.
- As a user, I want to be able to delete an activity from my
  daily planner.
- As a user, I want to be able to view an overview of my day,
  displayed hour-by-hour.
- As a user, I want to be able to update an activity by changing
  its name or adding a short description to it.
- As a user, I want to be able to view a list of only scheduled
  activities with their descriptions.
- As a user, I want to be able to have my DailyPlanner automatically save today's file.
- As a user, I want to be able to have my DailyPlanner automatically load today's file.
- As a user, I want to be able to save my DailyPlanner with the option to choose a file name.
- As a user, I want the option to be able to load a DailyPlanner 
  from another day from file.

## Phase 4: Task 2
```
Sat Nov 20 14:01:20 PST 2021
Added activity CPSC 210 project at 14
Sat Nov 20 14:01:30 PST 2021
Changed details of activity at 14 to Complete phase four
Sat Nov 20 14:01:33 PST 2021
Added activity Dinner at 16
Sat Nov 20 14:01:43 PST 2021
Added activity Biology project at 18
Sat Nov 20 14:01:52 PST 2021
Added activity Go out on a walk at 20
Sat Nov 20 14:02:03 PST 2021
Changed details of activity at 20 to If the weather isn't too bad...
Sat Nov 20 14:02:37 PST 2021
Changed details of activity at 20 to Weather is too bad, cancelling.
Sat Nov 20 14:02:40 PST 2021
Removed activity at: 20
```
## Phase 4: Task 3
After creating this UML design diagram, the most striking
thing that stood out to me is that there are no bidirectional
relationships within my project. As part of Phase 2, while
implementing Data Persistence I introduced a new field to
Activity to represent information associated with one
DailyPlanner (but not including the actual DailyPlanner itself) - 
if I had more time, I would focus on refactoring
the Activity and DailyPlanner relationship to be bidirectional.
To do this, I would:
- Introduce a new field to Activity for the DailyPlanner
- Remove the previous field representing DailyPlanner data in Activity
- Modify the addActivity and removeActivity methods in DailyPlanner
  so that the bidirectional relationship is maintained

Something else I noticed was that the DailyPlannerPanel and
DailyPlannerUI both have a lot of coupling as they reference
the same DailyPlanner twice. To fix this, I would:
- Add a getter to the DailyPlannerPanel to get the associated DailyPlanner
- Remove the unidirectional association between DailyPlannerUI and
  DailyPlanner by getting rid of the field in DailyPlannerUI for
  a DailyPlanner object

Another small change I would make is making subclasses of the
DailyPlannerUI (e.g. AddActivityAction) private, since they
aren't used anywhere else in the code.

Finally, I would have liked to incorporate the Observer pattern
into my UI design - currently, every time an action is performed
on the DailyPlanner a brand new DailyPlannerPanel has to be
generated; it would be much better with:
- DailyPlanner as a Subject
- DailyPlannerPanel as the Observer